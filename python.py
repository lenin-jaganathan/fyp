import pandas as pd
import numpy as np
from textblob.classifiers import NaiveBayesClassifier
from textblob import TextBlob

df = pd.read_csv('/home/local/ZOHOCORP/kavin-pt2609/fyp/ouputs/categorized.csv')
train = [
    ('I love this sandwich.', 'pos'),
    ('This is an amazing place!', 'pos'),
    ('I feel very good about these beers.', 'pos'),
    ('This is my best work.', 'pos'),
    ("What an awesome view", 'pos'),
    ('I do not like this restaurant', 'neg'),
    ('I am tired of this stuff.', 'neg'),
    ("I can't deal with this", 'neg'),
    ('He is my sworn enemy!', 'neg'),
    ('My boss is horrible.', 'neg')
]
cl = NaiveBayesClassifier(train)

print('Classifier trained')
df["tweet_sentiment"] = cl.classify(df["tweet"])
df.to_csv('final.csv')