import pandas as pd
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from keras.preprocessing.sequence import pad_sequences
from sklearn import svm
import pickle
from sklearn import model_selection, preprocessing, linear_model, naive_bayes, metrics, svm
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn import decomposition, ensemble

import xgboost
import textblob
import string
from keras.preprocessing import text, sequence
from keras import layers, models, optimizers


# import nltk
# from nltk.corpus import stopwords
# set(stopwords.words('english'))


def train_model(classifier, feature_vector_train, label, feature_vector_valid, is_neural_net=False,filename='1.pickle'):
    # fit the training dataset on the classifier
    classifier.fit(feature_vector_train, label)
    file = open(filename, 'wb')
    pickle.dump(classifier, file)

    # predict the labels on validation dataset
    predictions = classifier.predict(feature_vector_valid)

    if is_neural_net:
        predictions = predictions.argmax(axis=-1)

    
    print("Recall Score  :",metrics.recall_score(valid_y, predictions, average=None))
    print("Precision :",metrics.precision_score(valid_y, predictions, average=None))
    print("F1 Score : ",metrics.f1_score(valid_y, predictions, average=None))
    return metrics.accuracy_score(predictions, valid_y)


trainDF = pd.read_csv('./ouputs/train.csv')
# trainDF = trainDF.reindex(np.random.permutation(trainDF.index))

# print(trainDF['label'])

# split the dataset into training and validation datasets
train_x, valid_x, train_y, valid_y = model_selection.train_test_split(
    trainDF['text'], trainDF['label'],test_size=0.2)

# label encode the target variable
encoder = preprocessing.LabelEncoder()
train_y = encoder.fit_transform(train_y)
valid_y = encoder.fit_transform(valid_y)

# ---------------------------- CountVectors  ----------------------------------

count_vect = CountVectorizer(analyzer='word', token_pattern=r'\w{1,}')
count_vect.fit(trainDF['text'])

# transform the training and validation data using count vectorizer object
xtrain_count = count_vect.transform(train_x)
xvalid_count = count_vect.transform(valid_x)

# ------------------------------- TfidfVectorizer ----------------------------

tfidf_vect = TfidfVectorizer(
    analyzer='word', token_pattern=r'\w{1,}', max_features=5000)
tfidf_vect.fit(trainDF['text'])
xtrain_tfidf = tfidf_vect.transform(train_x)
xvalid_tfidf = tfidf_vect.transform(valid_x)

# ngram level tf-idf
tfidf_vect_ngram = TfidfVectorizer(
    analyzer='word', token_pattern=r'\w{1,}', ngram_range=(1,3), max_features=5000,stop_words='english')
tfidf_vect_ngram.fit(trainDF['text'])
xtrain_tfidf_ngram = tfidf_vect_ngram.transform(train_x)
xvalid_tfidf_ngram = tfidf_vect_ngram.transform(valid_x)

# accuracy = train_model(naive_bayes.MultinomialNB(), xtrain_count, train_y, xvalid_count,filename='nbCount.pickle')
# print("NB, Count Vectors: ", accuracy)

# # Naive Bayes on Word Level TF IDF Vectors
# accuracy = train_model(naive_bayes.MultinomialNB(), xtrain_tfidf, train_y, xvalid_tfidf)
# print("NB, WordLevel TF-IDF: ", accuracy)

accuracy = train_model(svm.SVC(gamma=0.01, C=1,kernel='linear',decision_function_shape='ovo'), xtrain_tfidf_ngram, train_y, xvalid_tfidf_ngram,filename='svm.pickle')
print("SVM, N-Gram Vectors: ", accuracy)


# accuracy = train_model(LogisticRegression(solver='lbfgs',multi_class='auto'), xtrain_tfidf_ngram, train_y, xvalid_tfidf_ngram,filename='lr.pickle')
# print("Logistic regression, N-Gram Vectors: ", accuracy)


# df = pd.read_csv('./ouputs/categorized.csv')
# tweet=df['tweet']
# # print (tweet.split(" "))
# print ("Creating the bag of words...\n")
# from sklearn.feature_extraction.text import CountVectorizer
# vectorizer = CountVectorizer()
# X= vectorizer.fit_transform(df['tweet'])
# Y = df['tweet_type']
# # model = svm.SVC()
# model = LogisticRegression()
# print('[INFO] Training Model')
# model.fit(X, Y)
# print('[INFO] Done')
# acc = model.score(X, Y)
# print(acc)
# X_Pred = vectorizer.fit_transform([['I need food for my child']])
# print(model.predict(X_Pred))

print("Classses  ----> ",encoder.inverse_transform([0,1,2,3,4,5]))

predicter = pickle.load(open('svm.pickle','rb'))

test = tfidf_vect_ngram.transform(['I need food for my children','Many people have died due to overflooding','We have collected food for over 1000 people and are travelling to affected area','Too many people have been suffering from medical needs','Many people are without houses or any other protective buildings'])

print(test.shape)

# print(pd.DataFrame(test.toarray()))

output = predicter.predict(test)

# print(output)

print('SVM',encoder.inverse_transform(output))


# predicter = pickle.load(open('lr.pickle','rb'))

# test = tfidf_vect_ngram.transform(['I need food for my children','Many people have died due to overflooding','We have collected food for over 1000 and are travelling to area','Too many people have been suffering from medical needs'])

# # print(test.shape)

# output = predicter.predict(test)

# print('Logistic Regression',encoder.inverse_transform(output))

