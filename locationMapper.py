from geopy.distance import geodesic
from geopy.geocoders import Nominatim

geolocator = Nominatim(user_agent="appName")

requestLocation = '13.0224, 80.2195'

print('User Location is :',geolocator.reverse(requestLocation))


availableLatLong = ["13.0827, 80.2707", "12.9830,80.2594","13.0101,80.2157"]
reverseLatLong = []

print('\n\nAvailable helpers are.....')
for index, data in enumerate(availableLatLong):
    # print(index, data)
    reverseLatLong.append(geolocator.reverse(data))

for data in reverseLatLong:
    print(data.address)

print('\n\n Distance between users ----> ')
for data in availableLatLong:
    print(geodesic(requestLocation,data))
# print(geodesic(newport_ri, cleveland_oh).miles)
# 538.390445368
