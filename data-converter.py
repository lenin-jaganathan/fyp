import os
import sys
import ast
import re
import string
import json
import nltk

words = set(nltk.corpus.words.words())

num_tweets_with_errors = 0

for line in open('./data/streamed-tweets.txt', 'r'):

    try:
        try:
            tweetdict = ast.literal_eval(line)
        except ValueError:
            try:
                tweetdict = json.loads(line)
            except ValueError as e:
                raise e

        tweetdict = json.loads(line)
        f = open("output.txt", "a")
        tweet_text = tweetdict['text'].replace('\n', ' ').encode('utf-8')
        " ".join(w for w in nltk.wordpunct_tokenize(tweet_text)
                 if w.lower() in words or not w.isalpha())
        f.write(tweet_text)
        f.write('\n')

    except SyntaxError:
        num_tweets_with_errors += 1
