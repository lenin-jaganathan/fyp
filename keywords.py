dictionary = {'food': [], 'water': [], 'shelter': [],
              'medicine': [], 'damage': [], 'electricity': []}

dictionary['water'] = ['drink', 'thirst',
                       'thirsty', 'dehydration', 'water', 'drinking']

dictionary['food'] = ['food', 'starve', 'hungry', 'milk', 'bread', 'eat', 'foodstuff',
                      'biscuits', 'snacks', 'cooking', 'cooked', 'eatables', 'fruits', 'vegetables']

dictionary['shelter'] = ['shelter', 'evacuation', 'house', 'living place', 'sleep',
                         'rest', 'accommodation', 'home', 'houseless', 'buildings', 'living place']

dictionary['medicine'] = ['medicine', 'clinic', 'hospital', 'pills', 'doctor', 'nurse', 'syrup',
                          'first aid', 'tonic', 'blood', 'first-aid', 'first aid', 'medical', 'sanitaion', 'prevention']

dictionary['electricity'] = ['electricity', 'power',
                             'electricity', 'fan', 'current', 'charge']

dictionary['damage'] = ['dead', 'injured', 'injury', 'affected', 'disease', 'destruction', 'damage', 'die', 'death', 'damaged',
                        'injuries', 'dead', 'injured', 'injury', 'killed', 'lost lives', 'fire', 'building', 'submerged', 'missed', 'lost']


disaster_keywords = {
    "hurricane": {
        "hurricane": .7,
        "flood": .2,
        "rain": .1
    },
    "earthquake":  {
        "earthquake": .7,
        "shake": .2,
        "shaking": .1
    }
}
