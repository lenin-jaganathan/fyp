from keywords import dictionary
# from stemming.porter2 import stem
import csv
import re

food_re_str = str()
for index, keyword in enumerate(dictionary['food']):
        food_re_str += '\\b' + keyword + '\\b'

        # add the "or" regex unless it is the last keyword for the disaster
        if index != len(dictionary['food']) - 1:
            food_re_str += '|'

food_re = re.compile(r'%s' % (food_re_str), re.IGNORECASE)



shelter_re_str = str()
for index, keyword in enumerate(dictionary['shelter']):
        shelter_re_str += '\\b' + keyword + '\\b'

        # add the "or" regex unless it is the last keyword for the disaster
        if index != len(dictionary['shelter']) - 1:
            shelter_re_str += '|'

shelter_re = re.compile(r'%s' % (shelter_re_str), re.IGNORECASE)


medicine_re_str = str()
for index, keyword in enumerate(dictionary['medicine']):
        medicine_re_str += '\\b' + keyword + '\\b'

        # add the "or" regex unless it is the last keyword for the disaster
        if index != len(dictionary['medicine']) - 1:
            medicine_re_str += '|'

medicine_re = re.compile(r'%s' % (medicine_re_str), re.IGNORECASE)


water_re_str = str()
for index, keyword in enumerate(dictionary['water']):
        water_re_str += '\\b' + keyword + '\\b'

        # add the "or" regex unless it is the last keyword for the disaster
        if index != len(dictionary['water']) - 1:
            water_re_str += '|'

water_re = re.compile(r'%s' % (water_re_str), re.IGNORECASE)



elect_re_str = str()
for index, keyword in enumerate(dictionary['electricity']):
        elect_re_str += '\\b' + keyword + '\\b'

        # add the "or" regex unless it is the last keyword for the disaster
        if index != len(dictionary['electricity']) - 1:
            elect_re_str += '|'

elect_re = re.compile(r'%s' % (elect_re_str), re.IGNORECASE)


damage_re_str = str()
for index, keyword in enumerate(dictionary['damage']):
        damage_re_str += '\\b' + keyword + '\\b'

        # add the "or" regex unless it is the last keyword for the disaster
        if index != len(dictionary['damage']) - 1:
            damage_re_str += '|'

damage_re = re.compile(r'%s' % (damage_re_str), re.IGNORECASE)




def categorize_tweets():

    tweets = []
    newDataset = {'food': [], 'water': [], 'shelter': [], 'medicine': [],'damage': [], 'electricity': []}

    food = []
    water = []
    damage = []
    medicine = []
    electricity = []
    shelter = []

    # with open('./output.txt', 'rU') as f:
    #     datareader = csv.reader(f, delimiter=',')
    #     for each in datareader:
    #         if(each):
    #             tweets.append(each)
    # categories = ['food', 'water', 'shelter', 'medicine', 'electricity']

    # for category in categories:
    #         weightCount = 0.0
    #         print(dictionary[category])

    weight = 0.2

    for tweet in open('./output.csv','r'):
        newArray = [3]
        newArray[0] = tweet
        tweet = newArray
        weightDict = {'food': 0, 'water': 0, 'shelter': 0, 'medicine': 0, 'electricity': 0, 'damage': 0}
        inputString = tweet[0]
        # print inputString
        for item in dictionary:
            # print item
            for keywords in dictionary[item]:
                # print keywords
                if ' '+keywords+'' in inputString:
                    weightDict[item] += weight
        maxVal = max(weightDict, key=weightDict.get)
        # print newDataset
        # print maxVal
        if(weightDict[maxVal] > 0):
            newDataset[maxVal].append(tweet)
        # print newDataset
        
        # if food_re.findall(inputString):
        #     tweet.append("food")
        #     newDataset.append(tweet)
        # if elect_re.findall(inputString):
        #     tweet.append("electricity")
        #     newDataset.append(tweet)
        # if water_re.findall(inputString):
        #     tweet.append("water")
        #     newDataset.append(tweet)
        # if medicine_re.findall(inputString):
        #     tweet.append("medicine")
        #     newDataset.append(tweet)
        # if shelter_re.findall(inputString):
        #     tweet.append("shelter")
        #     newDataset.append(tweet)
        # if damage_re.findall(inputString):
        #     tweet.append("damage")
        #     newDataset.append(tweet)
        # for category in categories:
        #     weightCount = 0.0
        #     synList = dictionary[category]

        #     if(category.lower() in inputString.lower()):
        #         weightCount += weight

        #     for each in synList:
        #         a = each.lower()
        #         if a[:-1] in inputString.lower():
        #             print a,inputString
        #             weightCount += weight

        #     for each in synList:
        #         a = each.lower()
        #         if stem(a[:-1]) in inputString.lower():
        #             weightCount += weight

        #     weightDict[category] = weightCount

        # maxCount = max(weightDict.values())

        # if(weightDict.values().count(maxCount) == 1):  # one group
        #     for key in weightDict.keys():
        #         if(weightDict[key] == maxCount):
        #             # category is the group with maximum weight
        #             tweet.append(key)
        # elif(maxCount == 0.0):    # no group
        #     tweet.append('Unknown')
        # else:  # multiple groups
        #     temp = []
        #     for key in weightDict.keys():
        #         if(weightDict[key] == maxCount):
        #             temp.append(key)

        #     tempString = ",".join(temp)
        #     tweet.append(tempString)

        # newDataset.append(tweet)

    with open('./ouputs/categorized.csv', "wb") as csvfile:
        areawriter = csv.writer(csvfile, delimiter=',')
        for l in newDataset:
            # print newDataset
            for item in newDataset[l]:
                item.append(l)
                areawriter.writerow(item)

    print "Categorized tweets into groups, e.g., food, water, etc: "


categorize_tweets()
